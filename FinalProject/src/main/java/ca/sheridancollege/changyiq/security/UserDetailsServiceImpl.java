package ca.sheridancollege.changyiq.security;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import ca.sheridancollege.changyiq.beans.Role;
import ca.sheridancollege.changyiq.repositories.EmployeeRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
	
	@Autowired
	private EmployeeRepository employeeRepository;


	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		
		// Find the custom user based on their email
		// ***** The User Class I have created *****
		ca.sheridancollege.changyiq.beans.Employee employee = employeeRepository.findByUsername(username);
		
		// If the user doesn't exist, throw an exception
		if (employee == null) {
			System.out.println("Employee not found:" + username);
			throw new UsernameNotFoundException("Employee " + username + " was not found in the database");
		}
		
		// Change the list of the user's roles into a list of GrantedAuthority
		// Convert our role entity into SpringBoot User Franted
		List<GrantedAuthority> grantList = new ArrayList<GrantedAuthority>(); 
		for (Role role : employee.getRoleList()) {
			grantList.add(new SimpleGrantedAuthority(role.getRolename())); 
		}
		
		// Create a Spring Boot “User” contained in a UserDetails based on the information above
		// ***** Create Another User Class of SpringBoot User, and cast it into UserDetails ******
		UserDetails userDetails = (UserDetails) new org.springframework.security.core.userdetails.User(
				employee.getEmployeeUsername(), 
				employee.getEncryptedPassword(), 
				grantList);
		return userDetails;

	}

}