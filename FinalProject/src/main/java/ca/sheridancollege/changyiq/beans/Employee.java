package ca.sheridancollege.changyiq.beans;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Entity(name="employee")
public class Employee {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NonNull
	private String employeeUsername; 
	
	@NonNull
	private String encryptedPassword;
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.EAGER) 
	private List<Role> roleList = new ArrayList<Role>();

	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(name="EMPLOYEE_DERMATOLOGIST_LIST", joinColumns=@JoinColumn(name="EMPLOYEE_ID"), inverseJoinColumns=@JoinColumn(name="DERMATOLOGIST_LIST_ID"))
	private List<Dermatologist> Dermatologistlist = new ArrayList<Dermatologist>();
	
	@ManyToMany(cascade=CascadeType.ALL, fetch = FetchType.LAZY) 
	@JoinTable(name="EMPLOYEE_USER_LIST", joinColumns=@JoinColumn(name="EMPLOYEE_ID"), inverseJoinColumns=@JoinColumn(name="USER_LIST_ID"))
	private List<User> userList = new ArrayList<User>();

}
