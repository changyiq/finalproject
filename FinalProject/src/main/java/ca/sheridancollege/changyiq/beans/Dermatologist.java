package ca.sheridancollege.changyiq.beans;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.OneToMany;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@RequiredArgsConstructor
@Builder
@Entity(name="dermatologist")
public class Dermatologist {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	@NonNull
	private String firstName;
	
	@NonNull
	private String lastName;
		
	@OneToMany(cascade=CascadeType.ALL)
	@JoinTable(name="DERMATOLOGIST_USER_LIST", joinColumns=@JoinColumn(name="DERMATOLOGIST_ID"), inverseJoinColumns=@JoinColumn(name="USER_ID"))
	private List<User> userList;

}
