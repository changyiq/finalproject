package ca.sheridancollege.changyiq.beans;

public enum UserIllnessLevel {

	MILD, MODERATE, SEVERE;
}
