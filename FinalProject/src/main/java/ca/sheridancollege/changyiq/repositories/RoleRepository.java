package ca.sheridancollege.changyiq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sheridancollege.changyiq.beans.Role;

public interface RoleRepository extends JpaRepository<Role, Long> {

}
