package ca.sheridancollege.changyiq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sheridancollege.changyiq.beans.Dermatologist;

public interface DermatologistRepository extends JpaRepository<Dermatologist, Long> {

}
