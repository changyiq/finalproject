package ca.sheridancollege.changyiq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sheridancollege.changyiq.beans.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

	public Employee findByUsername(String name);


}