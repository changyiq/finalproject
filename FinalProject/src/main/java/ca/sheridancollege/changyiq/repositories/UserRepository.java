package ca.sheridancollege.changyiq.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import ca.sheridancollege.changyiq.beans.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
