INSERT INTO Role(rolename) VALUES 
	('ROLE_USER'),
	('ROLE_GUEST');

INSERT INTO Employee(employee_username, encrypted_Password) VALUES
	('Mia', '$2a$10$1ltibqiyyBJMJQ4hqM7f0OusP6np/IHshkYc4TjedwHnwwNChQZCy'),
	('Alison', '$2a$10$1ltibqiyyBJMJQ4hqM7f0OusP6np/IHshkYc4TjedwHnwwNChQZCy'),
	('Oscar', '$2a$10$PrI5Gk9L.tSZiW9FXhTS8O8Mz9E97k2FZbFvGFFaSsiTUIl.TCrFu');

INSERT INTO Dermatologist(first_name, last_name) VALUES 
	('Mattie', 'Arnold'),
	('Sara', 'Long'),
	('Kyler', 'Palmer');
	
INSERT INTO User(username, age, symptoms, appointment_date, appointment_time, user_illness_level) VALUES
	('nicci_ogalla', 16, 'Acne, feels painful and itchy','2021-01-01', '14:45', 'MODERATE'),
	('marythomas76', 22, 'Small red, tender bumps','2021-01-20', '10:30', 'MILD'),
	('abjectdeanna',17, 'Large, solid, painful lumps under the skin','2020-12-01', '15:50', 'SEVERE');
	
INSERT INTO Dermatologist_User_List VALUES
	(1,1),
	(2,2),
	(3,3);

INSERT INTO Employee_Dermatologist_List VALUES
	(1,1),
	(1,2),
	(2,3);
	
INSERT INTO Employee_User_List VALUES
	(3,1),
	(1,2),
	(2,3);
	
INSERT INTO Employee_Role_List VALUES 
	(1, 1),
	(1, 2), 
	(2, 2),
	(3, 2);